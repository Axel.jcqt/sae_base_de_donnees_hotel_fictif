pause Suppression du contenu de la table PERSONNEL; /*cette premiere partie permet de faire déroulé le script petit à petit en appuyant sur entré pour mieux voir les erreurs*/
delete from PERSONNEL;
pause Suppression du contenu de la table RESERVER;
delete from RESERVER;
pause Suppression du contenu de la table COMMANDE;
delete from COMMANDE;
pause Suppression du contenu de la table CLIENT;
delete from CLIENT;
pause Suppression du contenu de la table PERSONNE;
delete from PERSONNE;
pause Suppression du contenu de la table DH;
delete from DH;
pause Suppression du contenu de la table CHAMBRE;
delete from CHAMBRE;
pause Suppression du contenu de la table PROPOSER;
delete from PROPOSER;
pause Suppression du contenu de la table SERVICES;
delete from SERVICES;
pause Suppression du contenu de la table DEPARTEMENT;
delete from DEPARTEMENT;
pause Suppression du contenu de la table HOTEL;
delete from HOTEL;

pause Insertion dans la table HOTEL;

insert into HOTEL (code,nom,ville,rue,gps,nbchambre,qualité)
values(10000001,'IBIS','Caen','rue du port','41.40338, 2.17403',20,4);
insert into HOTEL (code,nom,ville,rue,gps,nbchambre,qualité)
values(10000002,'IBIS','Orléans','quai du roi','40.40374, 3.18403',30,5);
insert into HOTEL (code,nom,ville,rue,gps,nbchambre,qualité)
values(10000003,'IBIS','Royan','rue du chien','45.40888, 7.38803',25,3);

pause Insertion dans la table DEPARTEMENT;

insert into DEPARTEMENT (code,nom,nomresponsable,numérotel)
values (10000001,'reception','Abel Auboisdormant',0620304050);
insert into DEPARTEMENT (code,nom,nomresponsable,numérotel)
values (10000001,'cuisine','Adam Troijours',0600000000);
insert into DEPARTEMENT (code,nom,nomresponsable,numérotel)
values (10000002,'reception','Adémar Monoto',0611111111);
insert into DEPARTEMENT (code,nom,nomresponsable,numérotel)
values (10000002,'bar','Adémar Okardetour',0622222222);
insert into DEPARTEMENT (code,nom,nomresponsable,numérotel)
values (10000003,'reception','Ela Tapéloui',0633333333);
insert into DEPARTEMENT (code,nom,nomresponsable,numérotel)
values (10000003,'piscine','Alain Verse',0644444444);

pause Insertion dans la table SERVICES;

insert into SERVICES (identifiant,nom,description,prix)
values(01,'kit de rassage','kit pour raser',04);
insert into SERVICES (identifiant,nom,description,prix)
values(02,'shampoing','kit de shampoing',05);
insert into SERVICES (identifiant,nom,description,prix)
values(03,'dentifrice et brosse a dent','pour votre hygiéne dentaire',10);
insert into SERVICES (identifiant,nom,description,prix)
values(04,'masque chirugical','pour vous proteger du covid',2);

pause Insertion dans la table PROPOSER;

insert into PROPOSER (code,identifiant)
values (10000001,01);
insert into PROPOSER (code,identifiant)
values (10000001,02);
insert into PROPOSER (code,identifiant)
values (10000001,04);
insert into PROPOSER (code,identifiant)
values (10000002,01);
insert into PROPOSER (code,identifiant)
values (10000002,04);
insert into PROPOSER (code,identifiant)
values (10000003,01);
insert into PROPOSER (code,identifiant)
values (10000003,02);
insert into PROPOSER (code,identifiant)
values (10000003,03);
insert into PROPOSER (code,identifiant)
values (10000003,04);

pause Insertion dans la table CHAMBRE;

insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000001,1,1,8);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000001,2,1,8);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000001,10,2,10);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000001,11,2,10);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000001,20,3,12);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000001,21,5,20);

insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000002,10,1,8);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000002,11,1,8);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000002,12,2,10);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000002,13,2,10);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000002,20,3,12);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000002,21,3,12);

insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000003,10,1,9);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000003,11,1,9);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000003,12,2,10);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000003,13,2,10);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000003,20,3,12);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000003,21,3,12);
insert into CHAMBRE (code,numchambre,nbmaxclient,superficie)
values (10000003,30,5,25);

pause Insertion dans la table DH;

insert into DH (dates,heure)
values (TO_DATE('2021/11/29', 'yyyy/mm/dd'),1510);
insert into DH (dates,heure)
values (TO_DATE('2021/11/30', 'yyyy/mm/dd'),1500);
insert into DH (dates,heure)
values (TO_DATE('2021/12/01', 'yyyy/mm/dd'),1400);
insert into DH (dates,heure)
values (TO_DATE('2021/12/01', 'yyyy/mm/dd'),1130);
insert into DH (dates,heure)
values (TO_DATE('2021/12/01', 'yyyy/mm/dd'),2010);
insert into DH (dates,heure)
values (TO_DATE('2021/12/02', 'yyyy/mm/dd'),1945);
insert into DH (dates,heure)
values (TO_DATE('2021/12/02', 'yyyy/mm/dd'),2140);
insert into DH (dates,heure)
values (TO_DATE('2021/12/12', 'yyyy/mm/dd'),1340);
insert into DH (dates,heure)
values (TO_DATE('2021/12/18', 'yyyy/mm/dd'),1545);

pause Insertion dans la table DH pour vérifier la contrainte sur l heure; /*test d'insertion d'une heure a 24H40*/
insert into DH (dates,heure) values (TO_DATE('2021/12/02', 'yyyy/mm/dd'),2440);

pause Insertion dans la table PERSONNE;

insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (65166816168484884684,'André Naline','13 Rue du Poireau Paris',TO_DATE('1999/02/10', 'yyyy/mm/dd'));
insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (68484616846846868468,'Anna Tomie','15 Rue du Comcombre Marseille',TO_DATE('1992/10/11', 'yyyy/mm/dd'));
insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (84848946848484849849,'Anne Héantie','22 Rue du Petit-Poit Lyon',TO_DATE('1977/08/20', 'yyyy/mm/dd'));
insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (84615651684616516846,'Armand Talo','456 Rue de la Carotte Toulouse',TO_DATE('1983/03/18', 'yyyy/mm/dd'));
insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (68498461848465184846,'Aubin Sahalor','7 Rue de la Patate Nice',TO_DATE('1996/06/13', 'yyyy/mm/dd'));
insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (84448949849849849849,'Aude Javel','6 Rue du Chou-Fleur Nantes',TO_DATE('1992/04/25', 'yyyy/mm/dd'));
insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (84984894984984984984,'Axel Ère','10 Rue du Poiveron Montpellier',TO_DATE('1997/09/19', 'yyyy/mm/dd'));
insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (65464646468484848489,'Beth Rave','113 Rue de la Tomate Strasbourg',TO_DATE('1993/03/01', 'yyyy/mm/dd'));
insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (65468486498498198499,'Bill Hattérahl','147 Rue de la Courgette Bordeaux',TO_DATE('1991/11/11', 'yyyy/mm/dd'));
insert into PERSONNE (numeroidentite,nom,adresse,datenaissance)
values (84981984984981984981,'Bowie Kent','11 Rue du Salsifie Lille',TO_DATE('1965/08/12', 'yyyy/mm/dd'));

pause Insertion dans la table CLIENT;

insert into CLIENT (numeroidentite,nationalite)
values (65166816168484884684,'Francaise');
insert into CLIENT (numeroidentite,nationalite)
values (84848946848484849849,'Francaise');
insert into CLIENT (numeroidentite,nationalite)
values (68498461848465184846,'Francaise');
insert into CLIENT (numeroidentite,nationalite)
values (84984894984984984984,'Francaise');
insert into CLIENT (numeroidentite,nationalite)
values (65468486498498198499,'Francaise');

pause Insertion dans la table COMMANDE;

insert into COMMANDE (code,numchambre,dates,heure,identifiant,numeroidentite,nationalite,déjapayé,quantité)
values (10000001,1,TO_DATE('2021/11/29', 'yyyy/mm/dd'),1510,1,65166816168484884684,'Francaise',1,01);
insert into COMMANDE (code,numchambre,dates,heure,identifiant,numeroidentite,nationalite,déjapayé,quantité)
values (10000001,11,TO_DATE('2021/11/30', 'yyyy/mm/dd'),1500,2,84848946848484849849,'Francaise',0,01);
insert into COMMANDE (code,numchambre,dates,heure,identifiant,numeroidentite,nationalite,déjapayé,quantité)
values (10000001,21,TO_DATE('2021/12/01', 'yyyy/mm/dd'),1400,3,84848946848484849849,'Francaise',0,01);

insert into COMMANDE (code,numchambre,dates,heure,identifiant,numeroidentite,nationalite,déjapayé,quantité)
values (10000002,10,TO_DATE('2021/12/01', 'yyyy/mm/dd'),1130,4,65166816168484884684,'Francaise',1,04);
insert into COMMANDE (code,numchambre,dates,heure,identifiant,numeroidentite,nationalite,déjapayé,quantité)
values (10000002,20,TO_DATE('2021/12/01', 'yyyy/mm/dd'),2010,3,68498461848465184846,'Francaise',1,01);
insert into COMMANDE (code,numchambre,dates,heure,identifiant,numeroidentite,nationalite,déjapayé,quantité)
values (10000002,21,TO_DATE('2021/12/02', 'yyyy/mm/dd'),1945,2,65468486498498198499,'Francaise',0,02);

insert into COMMANDE (code,numchambre,dates,heure,identifiant,numeroidentite,nationalite,déjapayé,quantité)
values (10000003,10,TO_DATE('2021/12/02', 'yyyy/mm/dd'),2140,2,68498461848465184846,'Francaise',1,01);
insert into COMMANDE (code,numchambre,dates,heure,identifiant,numeroidentite,nationalite,déjapayé,quantité)
values (10000003,12,TO_DATE('2021/12/12', 'yyyy/mm/dd'),1340,4,84984894984984984984,'Francaise',1,03);
insert into COMMANDE (code,numchambre,dates,heure,identifiant,numeroidentite,nationalite,déjapayé,quantité)
values (10000003,21,TO_DATE('2021/12/18', 'yyyy/mm/dd'),1545,1,65468486498498198499,'Francaise',0,01);

pause Insertion dans la table RESERVER;

insert into RESERVER (code,numchambre,numeroidentite,nationalite,dates,heure,nbaccompagnant,nbenfant,nbadulte,datedepart,prix)
values (10000001,1,65166816168484884684,'Francaise',TO_DATE('2021/11/29', 'yyyy/mm/dd'),1510,0,0,1,TO_DATE('2021/12/01', 'yyyy/mm/dd'),45);
insert into RESERVER (code,numchambre,numeroidentite,nationalite,dates,heure,nbaccompagnant,nbenfant,nbadulte,datedepart,prix)
values (10000001,11,84848946848484849849,'Francaise',TO_DATE('2021/11/30', 'yyyy/mm/dd'),1500,1,1,1,TO_DATE('2021/12/01', 'yyyy/mm/dd'),60);
insert into RESERVER (code,numchambre,numeroidentite,nationalite,dates,heure,nbaccompagnant,nbenfant,nbadulte,datedepart,prix)
values (10000002,20,68498461848465184846,'Francaise',TO_DATE('2021/12/01', 'yyyy/mm/dd'),2010,2,1,2,TO_DATE('2021/12/02', 'yyyy/mm/dd'),70);
insert into RESERVER (code,numchambre,numeroidentite,nationalite,dates,heure,nbaccompagnant,nbenfant,nbadulte,datedepart,prix)
values (10000003,12,84984894984984984984,'Francaise',TO_DATE('2021/12/12', 'yyyy/mm/dd'),1340,1,0,2,TO_DATE('2021/12/13', 'yyyy/mm/dd'),60);
insert into RESERVER (code,numchambre,numeroidentite,nationalite,dates,heure,nbaccompagnant,nbenfant,nbadulte,datedepart,prix)
values (10000002,21,65468486498498198499,'Francaise',TO_DATE('2021/12/18', 'yyyy/mm/dd'),1545,2,2,1,TO_DATE('2021/12/21', 'yyyy/mm/dd'),70);

pause Insertion dans la table PERSONNEL;

insert into PERSONNEL (code,nom,numeroidentite,fonction)
values (10000001,'reception',68484616846846868468,'Receptionniste');
insert into PERSONNEL (code,nom,numeroidentite,fonction)
values (10000002,'bar',84615651684616516846,'Barman');
insert into PERSONNEL (code,nom,numeroidentite,fonction)
values (10000002,'bar',84448949849849849849,'Service');
insert into PERSONNEL (code,nom,numeroidentite,fonction)
values (10000003,'piscine',65464646468484848489,'Sauveteur');
insert into PERSONNEL (code,nom,numeroidentite,fonction)
values (10000001,'cuisine',84981984984981984981,'Cuisinnier');
